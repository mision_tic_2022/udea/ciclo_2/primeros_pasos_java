
package com.mycompany.holamundo;

import java.util.Scanner;

public class HolaMundo {

    //Método principal
    public static void main(String[] args) {
        //Esto es un comentario de una sola linea
        /*Esto es un
        comentario de 
        varias lineas
         */
        //Mostrar mensaje en consola con salto de linea
        System.out.println("hola mundo");
        System.out.print("Otro mensaje\n");
        //Mostrar mensaje en consola sin salto de linea
        System.out.print("Mi nombre es Andres");
        /**
         * ***********************
         * VARIABLES
         ***********************
         */
        int entero = 5;
        Double decimal = 5.2;
        Double suma = entero + decimal;
        String saludo = "Hola mundo";
        Boolean booleano = true;
        var variable_dinamica = "Hola mundo";
        System.out.println(variable_dinamica);
        System.out.println(suma);
        System.out.println(saludo);
        System.out.println(booleano);

        //Llamar funciónes
        String nombre = "Andrés";
        saludar(nombre);

        int resultado = sumar(10,20);
        System.out.println("Resultado: "+resultado);

        double resp = restar(10.5, 5.2);
        System.out.println("Restra: "+resp);

        String nombre_completo = concatenar("Andrés", " Quintero");
        System.out.println(nombre_completo);

        boolean mayor = mayor_que_10(5);
        System.out.println("5 Es mayor que 10: "+mayor);
        mayor = mayor_que_10(10);
        System.out.println("10 Es mayor que 10: "+mayor);
        mayor = mayor_que_10(12);
        System.out.println("12 Es mayor que 10: "+mayor);

        //Llamar funciones que solicitan datos por consola
        //solicitar_nombre();
        //solicitar_enteros();
        //solicitar_decimales();
        multiplicar();
    }

    //Función que no retorna datos
    public static void saludar(String nombre){
        System.out.println("Hola "+nombre);
    }

    //Función que retorna un entero
    public static int sumar(int n1, int n2){
        int suma = n1+n2;
        return suma;
    }

    //Función que retorna un double
    public static double restar(double n1, double n2){
        return n1-n2;
    }

    //Función que retorna un String
    public static String concatenar(String nombre, String apellido){
        return nombre+apellido;
    }

    //Función que retorna un booleano
    public static boolean mayor_que_10(double n){
        return (n > 10);
    }

    public static void solicitar_nombre(){
        Scanner leer = new Scanner(System.in);
        System.out.print("Ingrese su nombre: ");
        //capturar datos en consola
        String nombre = leer.next();
        saludar(nombre);
    }

    public static void solicitar_enteros(){
        Scanner leer = new Scanner(System.in);
        System.out.print("Ingrese primer número mayor que 10: ");
        int n1 = leer.nextInt();
        if(mayor_que_10(n1) == true){
            System.out.print("Ingrese el segundo número: ");
            int n2 = leer.nextInt();
            int suma = sumar(n1, n2);
            System.out.println("La suma es: "+suma);
        }else{
            System.out.println("Debe ingresar un número mayor que 10");
        }
        
    }

    public static void solicitar_decimales(){
        Scanner leer = new Scanner(System.in);
        System.out.print("Ingrese primer número con decimales mayor que 10: ");
        double n1 = leer.nextDouble();
        if(mayor_que_10(n1)){
            System.out.print("Ingrese segundo número con decimales: ");
            double n2 = leer.nextDouble();
            double resta = restar(n1,n2);
            System.out.println("Resta: "+resta);
        }else{
            System.out.println("Debe ingresar un numero mayor que 10");
        }
    }

    public static void multiplicar(){
        Scanner leer = new Scanner(System.in);
        System.out.print("Ingrese primer número: ");
        double n1 = leer.nextDouble();
        System.out.print("Ingrese segundo número: ");
        double n2 = leer.nextDouble();
        //Declarar variable
        double multiplicacion;
        //Condicional
        if( (n1 > 0 && n1 < 100 && n1 != 10) ){
            multiplicacion = n1*n2;
        }else{
            multiplicacion = 0;
        }

        System.out.println("Multiplicación: "+multiplicacion);
    }

}
